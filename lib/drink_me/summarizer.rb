module DrinkMe
  class Summarizer
    DEFAULT_SENTENCES = 4

    # Initializes a Summarizer object
    # @param text [String] the text that will be summarized
    # @param sentences [Integer] the number of sentences the summary output will have. Default: {DEFAULT_SENTENCES}
    # @param top_words_count [Integer] the number of top words that the scorer algorithm will count. Default: {WordCounter::DEFAULT_TOP_WORDS_COUNT}
    def initialize(text:, sentences: DEFAULT_SENTENCES, top_words_count: WordCounter::DEFAULT_TOP_WORDS_COUNT)
      @text            = text.to_s
      @sentences       = sentences
      @top_words_count = top_words_count
    end

    # @return [String] the summarized text
    def summarize
      create_summary
    end

    private

    def scorer
      @scorer ||= DrinkMe::Scorer.new(text: @text,
                                      options: {
                                        top_words_count: @top_words_count
                                      })
    end

    def scores
      scorer.scores
    end

    def create_summary
      ranking = sentence_ranking

      scores[:sentences].select.with_index do |_sentence, index|
        ranking.include? index
      end.join(' ')
    end

    def sentence_ranking
      scores[:sentence_scores]
        .sort_by { |_index, score| score }
        .reverse
        .map { |sentence| sentence[0] }[0...@sentences]
    end
  end
end
