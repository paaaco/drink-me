module DrinkMe
  class Scorer
    def initialize(text: '', options: {})
      @text    = text
      @options = options
    end

    def scores
      {
        sentences: split_sentences,
        sentence_scores: score_sentences,
        top_words: top_words
      }
    end

    private

    def top_words
      @top_words ||= WordCounter.new(words: split_words, **@options).top_words
    end

    def split_words
      @text.split(' ')
    end

    def split_sentences
      PragmaticSegmenter::Segmenter
        .new(text: @text)
        .segment
    end

    def score_sentences
      sentences       = split_sentences
      sentence_scores = Hash.new(0)

      sentences.each_with_index do |s, index|
        s.split(' ').each do |word|
          sentence_scores[index] += top_words.fetch(word.downcase, 0)
        end
      end

      sentence_scores
    end
  end
end
