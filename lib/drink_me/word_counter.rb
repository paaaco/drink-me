require 'drink_me/stop_words'

module DrinkMe
  include StopWords

  class WordCounter
    attr_reader :top_words

    DEFAULT_TOP_WORDS_COUNT = 50

    def initialize(words: [], top_words_count: DEFAULT_TOP_WORDS_COUNT)
      @words           = words
      @top_words_count = top_words_count
      @top_words       = top_words_list
    end

    private

    def count_words
      word_counts = Hash.new(0)
      @words.each do |word|
        unless StopWords::ENGLISH.include? word.downcase
          word_counts[word.downcase] += 1
        end
      end
      word_counts
    end

    def top_words_list
      words = Hash.new(0)

      count_words
        .sort_by { |_key, val| val }
        .reverse[0...@top_words_count]
        .each do |word_counts|
          word, count = word_counts
          words[word] = count
        end

      words
    end
  end
end
