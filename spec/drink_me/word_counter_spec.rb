require 'spec_helper'

describe DrinkMe::WordCounter do
  let(:text) { load_text }
  let(:options) { { words: text.split(' ') } }

  before do
    text = load_text
    @word_counter = DrinkMe::WordCounter.new(options)
  end

  describe '#count_words' do
    subject do
      @word_counter.send(:count_words)
    end

    it 'should return a hash of words used with corresponding count' do
      expect(subject).to have_key('government')
      expect(subject['government']).to eq 5
    end

    it 'should not return common stop words' do
      expect(subject).to_not have_key 'the'
      expect(subject).to_not have_key 'a'
      expect(subject).to_not have_key 'if'
    end
  end

  describe '#top_words' do
    subject do
      @word_counter.send(:top_words)
    end

    context 'default options' do
      it 'should return the top 50 common words in the text' do
        expect(subject.size).to eq 50
      end
    end

    context 'top_words_count is changed' do
      let(:options) { { words: text.split(' '), top_words_count: 3 } }

      it 'should change the number of returned common words' do
        expect(subject.size).to eq 3
      end
    end
  end
end
