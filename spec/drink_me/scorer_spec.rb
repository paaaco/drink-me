require 'spec_helper'

describe DrinkMe::Scorer do
  before do
    text    = load_text
    @scorer = DrinkMe::Scorer.new(text: text)
  end

  describe '#scores' do
    it 'should return a hash' do
      expect(@scorer.scores).to be_a Hash
    end

    it 'should contain the top words of the text' do
      expect(@scorer.scores).to have_key :top_words
    end

    it 'should contain the sentence scores of the text' do
      expect(@scorer.scores).to have_key :sentence_scores
    end
  end

  describe '#split_sentences' do
    subject do
      @scorer.send(:split_sentences)
    end

    it 'should return an array of sentences' do
      expect(subject).to be_a Array
    end

    it 'should have each sentence as an element' do
      expect(subject.size).to eq 7
    end
  end

  describe '#score_sentences' do
    subject do
      @scorer.send(:score_sentences)
    end

    it 'should return a hash of sentence scores' do
      expect(subject).to be_a Hash
    end

    it 'each sentence should have an entry in the hash' do
      expect(subject.keys.count).to eq @scorer.send(:split_sentences).count
    end

    it 'each sentence should be scored based on how many of the common words it has' do
      expect(subject[0]).to eq 18
    end
  end
end
