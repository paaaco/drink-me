require 'spec_helper'

describe DrinkMe::Summarizer do
  let(:text) { load_text }
  let(:options) { { text: text } }

  describe 'instantiation' do
    subject { DrinkMe::Summarizer.new(options) }
    context 'when given :text argument' do
      it 'should return a Summarizer object' do
        expect(subject).to be_a DrinkMe::Summarizer
      end
    end

    context 'when given :sentences argument' do
      let(:options) { { text: text, sentences: 6 } }
      it 'should change the number of sentences to be outputted' do
        summarizer = subject
        expect(summarizer.instance_variable_get('@sentences')).to eq 6
      end
    end

    context 'when given :top_words_count argument' do
      let(:options) { { text: text, top_words_count: 100 } }
      it 'should pass this argument to the word counter class' do
        expect(DrinkMe::WordCounter).to(
          receive(:new)
            .with(hash_including(top_words_count: 100))
            .at_least(:once)
            .and_call_original
        )
        summarizer = subject
        summarizer.summarize
      end
    end

    context 'when given no arguments' do
      let(:options) { {} }
      it 'should raise an error' do
        expect { subject }.to raise_error ArgumentError
      end
    end
  end

  describe 'methods' do
    before do
      @summarizer = DrinkMe::Summarizer.new(options)
    end

    describe '#summarize' do
      it 'should return summarized version of the text' do
        expect(@summarizer.summarize).to be_a String
        expect(@summarizer.summarize.size).to be < text.size
      end

      context 'when given a non-string input' do
        let(:options) { { text: 10 } }
        it 'should convert text argument to String first' do
          expect(@summarizer.summarize).to eq '10'
        end
      end
    end

    describe '#create_summary' do
      subject do
        @summarizer.send(:create_summary)
      end

      it 'should combine the top-scoring sentences' do
        expect(subject).to be_a String
      end
    end

    describe '#sentence_ranking' do
      subject do
        @summarizer.send(:sentence_ranking)
      end

      context 'default options' do
        it 'should return an array of the indices of the top scoring sentences' do
          expect(subject).to be_a Array
        end

        it 'should return the default number of sentence indices (4)' do
          expect(subject.size).to eq 4
        end
      end

      context 'when sentence count option is altered' do
        let(:options) { { text: text, sentences: 2 } }

        it 'should only return two sentences' do
          expect(subject.size).to eq 2
        end
      end
    end
  end

  describe 'feature testing' do
    it 'should summarize a given text' do
      text = load_text
      text_summarized = load_text(filename: 'test_article_summarized.txt')
      s = DrinkMe::Summarizer.new(text: text)

      expect(s.summarize).to eq(text_summarized.chomp)
    end
  end
end
