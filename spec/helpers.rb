module Helpers
  def load_text(filename: 'test_article.txt')
    path = "spec/text_files/#{filename}"
    File.open(path).read
  end
end
