Gem::Specification.new do |s|
  s.name = 'drink_me'
  s.version = '0.0.1'
  s.date = '2018-03-23'
  s.summary = 'Text summarizer for Ruby'
  s.description = 'A gem that will summarize text while still retaining the most significant information'
  s.authors = ['Paco Halili']
  s.email = 'pacohalili@mac.com'
  s.files = Dir['{lib}/**/*.rb', 'bin/*', 'LICENSE', '*.md']
  s.require_path = 'lib'
  s.homepage = 'http://gitlab.com/paaaco/drink-me'
  s.license = 'MIT'
end
