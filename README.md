# Drink Me

> This time she found a little bottle on it , and tied 'round the neck of the
bottle was a paper label, with the words "DRINK ME" beautifully printed
on it in large letters.

> [S]o Alice ventured
to taste it, and, finding it very nice, she very soon finished it off.

> She was now only ten inches high, and her face
brightened up at the thought that she was now the right size for going
through the little door into that lovely garden.




**Drink Me** is a gem that will summarize long text like articles while still keeping significant information.

## Getting Started

### Installing

Install this gem through

`gem drink_me, git: "git@gitlab.com:paaaco/drink-me.git"`

### Basic Usage

``` rb
require 'drink_me' # Require the gem

text = %( The text that you want to summarize )

s = DrinkMe::Summarizer.new(text: text)
s.summarize
# => "summarized text"
```

### Additional Options

``` rb
s = DrinkMe::Summarizer.new(
      text: text,
      sentences: 3, # number of sentences to output. default is 4
      top_words_count: 100 # number of high-frequency words to record for the scoring. default is 50
    )
```

## Built With

* [Pragmatic Segmenter](https://github.com/diasks2/pragmatic_segmenter) - used for splitting up sentences

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
